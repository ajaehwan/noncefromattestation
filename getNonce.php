<html>
<head>
<title>php example </title>
</head>
<body>
<h1>Get a nonce from the Attestation server. </h1>
<p>
<?php
// create a new cURL resource
$ch = curl_init();
// set URL and other appropriate options
curl_setopt($ch, CURLOPT_URL, "https://attest-api.secb2b.com/v2/nonces");
//curl_setopt($ch, CURLOPT_HEADER, 0);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, false);
$headers = array(
        'x-Knox-attest-api-key: '."AABBCCDD...",    // should change the key value such as "AABBCC..." with a real one.
        'cache-control'."no-cache"
);
curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
// grab URL and pass it to the browser
curl_exec($ch);
// close cURL resource, and free up system resources
curl_close($ch);
?>
</p>
</body>
</html>