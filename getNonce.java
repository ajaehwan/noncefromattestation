// Library dependency 

// OkHttp jar should be used. The file exist in "https://square.github.io/okhttp/"
// Download the file "okhttp-3.13.1.jar" and copy this file in the folder "libs" of the java project.
// Also download the file "okio-2.2.2.jar" and do the job like above.
// Also download the kotlin library in "https://jar-download.com/artifact-search/kotlin-stdlib" (3 files) and do the job like above.

// In the right click menu in eclipse, Build Path -> Configure Build Path -> Libraries -> Add Jars -> "select the jar file" -> apply
// That's all.

import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class GetExample {	
	public static final MediaType JSON = MediaType.get("application/json; charset=utf-8");
  OkHttpClient client = new OkHttpClient();
  
  String json = "";
  
  String run() throws IOException {
	  RequestBody body = RequestBody.create(JSON, json);
	  Request request = new Request.Builder()
			  .url("https://attest-api.secb2b.com/v2/nonces")
			  .post(body)
			  .addHeader("x-knox-attest-api-key", "AABBCCDDEEFF...")  // should change the key value such as "AABBCC..."
			  .addHeader("cache-control", "no-cache")
			  .build();


	  try (Response response = client.newCall(request).execute()) {
		  return response.body().string();
    }
  }

  public static void main(String[] args) throws IOException {
    GetExample example = new GetExample();
    String response = example.run();
	
	  System.out.println(response);
  }
}