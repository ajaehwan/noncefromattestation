package attestation;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class GetVerdict {
	public static final MediaType OCTET = MediaType.get("application/octet-stream");
	  OkHttpClient client = new OkHttpClient();
	  int responsecode;
	  byte[] blob=null;
	  
	  GetVerdict() {
		  //String blob = "hhh";
		  String blob_filename = "blob.bin";
		  File iFile = new File(blob_filename);
		  
		//  File tmppath = new File(".");
		//  System.out.println("current location : " + tmppath.getAbsolutePath());
		  
		  if(iFile.exists())
		  {
			  FileInputStream in=null;
			  long filesize = iFile.length();
			  try { 
				  in = new FileInputStream(blob_filename);
				  blob = new byte[(int)filesize];
				  
				  in.read(blob);
				  
			  } catch(FileNotFoundException fnfe) {
				  System.out.println(blob_filename + "File not found.");
			  } catch(IOException ioe) {
				  System.out.println(blob_filename + "Cannot read file.");
			  } finally {
				  try {
					  in.close();
				  }
				  catch(Exception e) {}
			  }
		  }	
		  else
		  {
			  System.out.println(blob_filename + ", I can't find blob file.");
		  }
	  }
	  
	  String run() throws IOException {
		  if(blob == null)
		  {
			  System.out.println("Since blob file does not exist, can't send request to Attestation server");
			  return null;
		  }
		  System.out.println("blob size : " + blob.length);
		
		  RequestBody body = RequestBody.create(OCTET, blob);
		  Request request = new Request.Builder()
				  .url("https://attest-api.secb2b.com/v2/blobs?nonce=1A7E565DD12B36C27856F8E586F62F24C664A5965B5D8833C11E3418C7A7AE8D")
				  .post(body)
				  .addHeader("x-knox-attest-api-key", "AABB...")  // should change the key value such as "AABBCC..."
				  .addHeader("cache-control", "no-cache")
				  .addHeader("Content-Type", "application/octet-stream")
				  .build();


		  try (Response response = client.newCall(request).execute()) {
			  responsecode = response.code();
			  return response.body().string();
	    }
	  }

	  public static void main(String[] args) throws IOException {
		  GetVerdict getverdict = new GetVerdict();
	    String response = getverdict.run();
		
	    if(response == null)
	    {
	    	System.out.println("No operation between client and server");
	    	return;
	    }
	    else {
		  System.out.println("The response from the Attestation Server:"+response);
		  System.out.println("The response code :" + getverdict.responsecode);
	    }
		  

	  }
}
